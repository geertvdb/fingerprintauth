package com.geertvdb.nzxt.fingerprintauth2;

import android.app.KeyguardManager;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private Button btnRegister;
    private KeyguardManager keyguardManager;
    private FingerprintManager fingerprintManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        imageView = (ImageView)findViewById(R.id.ivImage);
        btnRegister = (Button) findViewById(R.id.btnRegisterFingerPrint);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(MainActivity.this, LoginActivity.class));

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
                finish();
            }
        });

        startFingerPrint();
    }

    private void startFingerPrint() {
        if(checkFingerPrintSettings()){
            Toast.makeText(this, "Put your finger on the sensor to login!", Toast.LENGTH_LONG).show();
            FingerPrintAuthenticator fingerPrintAuthenticator = FingerPrintAuthenticator.getInstance();

            if(fingerPrintAuthenticator.cipherInit()){
                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(fingerPrintAuthenticator.getCipher());
                FingerPrintHandler helper = new FingerPrintHandler(MainActivity.this);
                helper.startAuthentication(fingerprintManager, cryptoObject);
            }
        };
    }

    private boolean checkFingerPrintSettings() {

        if(!fingerprintManager.isHardwareDetected()){
            Toast.makeText(this, "Fingerprint authentication permission not enable", Toast.LENGTH_LONG).show();
        }else{
            if(!fingerprintManager.hasEnrolledFingerprints()){
                Toast.makeText(this, "Register at least one fingerprint", Toast.LENGTH_LONG).show();
                btnRegister.setVisibility(View.VISIBLE);
                //startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            }else{
                if(keyguardManager.isDeviceSecure()){
                    return true;
                }
            }
        }

        return false;
    }
}
